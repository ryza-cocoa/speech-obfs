//
//  AppDelegate.m
//  Speech Obfs
//
//  Created by Ryza on 11/02/2020.
//  Copyright © 2020 Ryza. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)applicationShouldHandleReopen:(NSApplication *)sender hasVisibleWindows:(BOOL)flag {
    if (!flag) {
        for (id const window in sender.windows) {
            [window makeKeyAndOrderFront:self];
        }
    }
    return YES;
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender {
    return NO;
}

@end
