//
//  SOWindow.m
//  Speech Obfs
//
//  Created by Ryza on 11/02/2020.
//  Copyright © 2020 Ryza. All rights reserved.
//

#import "SOWindow.h"

@implementation SOWindow

- (instancetype)initWithContentRect:(NSRect)contentRect styleMask:(NSWindowStyleMask)style backing:(NSBackingStoreType)backingStoreType defer:(BOOL)flag {
    self = [super initWithContentRect:contentRect styleMask:style backing:backingStoreType defer:flag];
    if (self) {
        self.opaque = NO;
        self.backgroundColor = [NSColor clearColor];
        self.movableByWindowBackground = YES;
    }
    return self;
}

@end
