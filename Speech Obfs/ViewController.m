//
//  ViewController.m
//  Speech Obfs
//
//  Created by Ryza on 11/02/2020.
//  Copyright © 2020 Ryza. All rights reserved.
//

#import "ViewController.h"
#import "SpeechObfs.h"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.originalTextView setFont:[NSFont fontWithName:@"Monaco" size:18.0f]];
    [self.obfsTextView setFont:[NSFont fontWithName:@"Monaco" size:18.0f]];
}

- (IBAction)obfs:(id)sender {
    [self.obfsTextView setString:[[SpeechObfs sharedInstance] obfuscate:self.originalTextView.string
                                       withObfsSymbol:self.enableObfsSymbolsButton.cell.state == NSControlStateValueOn]];
}

@end
