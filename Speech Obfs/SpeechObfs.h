//
//  SpeechObfs.h
//  Speech Obfs
//
//  Created by Ryza on 11/02/2020.
//  Copyright © 2020 Ryza. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SpeechObfs : NSObject
+ (instancetype)sharedInstance;
- (NSString *)obfuscate:(NSString *)original withObfsSymbol:(BOOL)enableObfsSymbol;
@end

NS_ASSUME_NONNULL_END
