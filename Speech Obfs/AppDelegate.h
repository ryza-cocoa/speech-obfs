//
//  AppDelegate.h
//  Speech Obfs
//
//  Created by Ryza on 11/02/2020.
//  Copyright © 2020 Ryza. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSWindowController <NSApplicationDelegate>

@end

