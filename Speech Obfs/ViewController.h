//
//  ViewController.h
//  Speech Obfs
//
//  Created by Ryza on 11/02/2020.
//  Copyright © 2020 Ryza. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ViewController : NSViewController

@property (strong) IBOutlet NSTextView *originalTextView;
@property (strong) IBOutlet NSTextView *obfsTextView;
@property (weak) IBOutlet NSButton *enableObfsSymbolsButton;
- (IBAction)obfs:(id)sender;

@end

