//
//  SpeechObfs.m
//  Speech Obfs
//
//  Created by Ryza on 11/02/2020.
//  Copyright © 2020 Ryza. All rights reserved.
//

#import "SpeechObfs.h"

@interface SpeechObfs()
@property (nonatomic, nullable) NSDictionary * dict;
@property (nonatomic, nullable) NSMutableArray * obfsSymbols;
@end

@implementation SpeechObfs

+ (instancetype)sharedInstance {
    static SpeechObfs * instance = nil;
    if (instance == nil) {
        instance = [[SpeechObfs alloc] init];
    }
    return instance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        NSError * error = nil;
        NSString * dictString = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"dict" ofType:@"json"] encoding:NSUTF8StringEncoding error:&error];
        if (error) {
            NSLog(@"Cannot find or read dict.json");
            exit(EXIT_FAILURE);
        }
        
        NSData *dictData = [dictString dataUsingEncoding:NSUTF8StringEncoding];
        self.dict = [NSJSONSerialization JSONObjectWithData:dictData options:NSJSONReadingMutableContainers error:&error];
        if (error) {
            NSLog(@"Cannot parse dict.json");
            exit(EXIT_FAILURE);
        } else {
            NSLog(@"%lu items loaded", self.dict.count);
        }
        
        self.obfsSymbols = [[NSMutableArray alloc] init];
        NSString * obfsSymbolString = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"obfs" ofType:@"txt"] encoding:NSUTF8StringEncoding error:&error];
        if (error) {
            NSLog(@"Cannot find or read obfs.txt");
        }
        [obfsSymbolString enumerateSubstringsInRange:NSMakeRange(0, [obfsSymbolString length])
                                             options:NSStringEnumerationByComposedCharacterSequences
                                          usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
            [self.obfsSymbols addObject:substring];
        }];
    }
    return self;
}

- (NSString *)obfuscate:(NSString *)original withObfsSymbol:(BOOL)enableObfsSymbol {
    __block NSMutableString * result = [NSMutableString string];
    [original enumerateSubstringsInRange:NSMakeRange(0, [original length])
                                 options:NSStringEnumerationByComposedCharacterSequences
                              usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
        NSArray * alternatives = [self.dict valueForKey:substring];
        if (alternatives) {
            [result appendString:[alternatives objectAtIndex:arc4random() % alternatives.count]];
        } else {
            [result appendString:substring];
        }
        
        // obfs
        if (enableObfsSymbol && self.obfsSymbols.count > 0 && rand() % 23 < 7) {
            [result appendString:[self.obfsSymbols objectAtIndex:arc4random() % self.obfsSymbols.count]];
        }
    }];
    
    return result;
}

@end
