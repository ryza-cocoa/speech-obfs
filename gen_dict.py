#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json

def gen_dict(original, mutation):
    original_file = open(original, "r")
    mutation_files = [open(mutation[i]) for i in range(len(mutation))]
    line_index = 0
    results = {}
    while True:
        orig = original_file.readline()
        if not orig:
            break
        else:
            orig = orig.strip().split(' ')
            mutations = [mutation_file.readline().strip().split(' ') for mutation_file in mutation_files]
            for index in range(len(orig)):
                results[orig[index]] = []
                for mutation_index in range(len(mutation)):
                    results[orig[index]].append(mutations[mutation_index][index])
                results[orig[index]] = list(dict.fromkeys(results[orig[index]]))
    return results

if __name__ == "__main__":
    result = json.dumps(gen_dict("blocks.txt", ["v1.txt", "v2.txt", "v3.txt", "v4.txt"]), indent=2, ensure_ascii=False)
    with open("dict.json", "w") as f:
        f.write(result)
