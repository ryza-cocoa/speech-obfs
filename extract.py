#!/usr/bin/python3
# -*- coding: utf-8 -*-

def extract(path, save):
    content = ""
    with open(path, "r") as f:
        for line in f:
            line = line.strip().replace(r'\s', '')
            content += line

    result = ""
    for word in content:
        result += word + ' '

    with open(save, "w") as f:
        start = 0
        length = 50
        index = 0
        while start < len(result):
            end = min([start + length, len(result)])
            current = result[start:end]
            if index % 19 == 0 and index != 0:
                f.write("\n")
            f.write(current)
            f.write("\n")
            index += 1
            start += length

if __name__ == "__main__":
    extract("words.txt", "blocks.txt")
