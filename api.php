<?php
    $content = mb_convert_encoding(@$_POST["content"], "UTF-8", "auto");
    $content_length = mb_strlen($content);
    $status = "error";
    $result = "";
    $error = "";
    
    if ($content_length != 0) {
        $dict = @json_decode(file_get_contents("dict.json"), true);
        if ($dict) {
            $obfs = mb_convert_encoding(@file_get_contents("obfs.txt"), "UTF-8", "auto");
            $obfs_length = mb_strlen($obfs);
            for ($i = 0; $i < $content_length; $i++) {
                $key = mb_substr($content, $i, 1);
                if (array_key_exists($key, $dict)) {
                    $alternatives = $dict[$key];
                    $count = count($alternatives);
                    $result .= $alternatives[rand() % $count];
                } else {
                    $result .= $key;
                }
                
                // obfs
                if ($obfs_length > 0 && rand() % 23 < 7) {
                    $result .= mb_substr($obfs, rand() % $obfs_length, 1);
                }
            }
            $status = "ok";
        } else {
            $error = "missing dict.json";
        }
    } else {
        $error = "no input content found";
    }
    
    $type = @$_POST["type"];
    if ($type == "json") {
        $json = array("status" => $status, "result" => $result);
        if ($status == "error") {
            $json["error"] = $error;
        }
        echo json_encode($json);
    } else {
        echo $result;
    }
?>
